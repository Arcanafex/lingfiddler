﻿using System;
using System.Collections.Generic;

namespace Epigrapher.Lx
{
    /// <summary>
    /// a set of Glyphs
    /// </summary>
    public class Script : Dictionary<string, Glyph>
    {
        private readonly Guid m_id;
        private readonly Dictionary<string, string> m_designations;
        private readonly Dictionary<string, Glyph> m_inventory;

        public Guid Id => m_id;
        public string Name { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> Designations => m_designations;

        // NOTE: Ordering of the set of glyphs is purely a function of orthography
        public Dictionary<string, Glyph> Inventory => m_inventory;

        public Script()
        {
            m_designations = new Dictionary<string, string>();
            m_inventory = new Dictionary<string, Glyph>();
        }

        public Glyph AddGlyph(string glyph)
        {
            if (ContainsKey(glyph))
            {
                return this[glyph];
            }
            else
            {
                var outGlyph = new Glyph(glyph);
                Add(glyph, outGlyph);
                return outGlyph;
            }
        }

        public List<Glyph> AddGlyphs(string[] glyphs)
        {
            var glyphList = new List<Glyph>();

            foreach (var glyph in glyphs)
            {
                glyphList.Add(AddGlyph(glyph));
            }

            return glyphList;
        }

        public List<Glyph> AddGlyphs(char[] glyphs)
        {
            var glyphList = new List<Glyph>();

            foreach (var glyph in glyphs)
            {
                glyphList.Add(AddGlyph(glyph.ToString()));
            }

            return glyphList;
        }
    }

}

