﻿using Epigrapher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epigrapher.Lx
{
    public enum SegmentType
    {
        Glyph,
        Graph,
        Word,
        Expression,
        Discourse,
        Text
    }

    public interface ISegment
    {
        string Graph { get; set; }

        //
        // Summary:
        //     Initializes a new instance of the System.Collections.Generic.LinkedListNode`1
        //     class, containing the specified value.
        //
        // Parameters:
        //   value:
        //     The value to contain in the System.Collections.Generic.LinkedListNode`1.
        //public LinkedListNode(T value);

        //
        // Summary:
        //     Gets the System.Collections.Generic.LinkedList`1 that the System.Collections.Generic.LinkedListNode`1
        //     belongs to.
        //
        // Returns:
        //     A reference to the System.Collections.Generic.LinkedList`1 that the System.Collections.Generic.LinkedListNode`1
        //     belongs to, or null if the System.Collections.Generic.LinkedListNode`1 is not
        //     linked.
        //public LinkedList<T> List { get; }
        //
        // Summary:
        //     Gets the next node in the System.Collections.Generic.LinkedList`1.
        //
        // Returns:
        //     A reference to the next node in the System.Collections.Generic.LinkedList`1,
        //     or null if the current node is the last element (System.Collections.Generic.LinkedList`1.Last)
        //     of the System.Collections.Generic.LinkedList`1.
        //public LinkedListNode<T> Next { get; }
        //
        // Summary:
        //     Gets the previous node in the System.Collections.Generic.LinkedList`1.
        //
        // Returns:
        //     A reference to the previous node in the System.Collections.Generic.LinkedList`1,
        //     or null if the current node is the first element (System.Collections.Generic.LinkedList`1.First)
        //     of the System.Collections.Generic.LinkedList`1.
        //public LinkedListNode<T> Previous { get; }
        //
        // Summary:
        //     Gets the value contained in the node.
        //
        // Returns:
        //     The value contained in the node.
        //public T Value { get; set; }

    }
}
