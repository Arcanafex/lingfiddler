﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Epigrapher.Lx
{
    public interface IGlyph
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }

        //Unicode
        int GeneralCategory { get; set; }
        int Characteristics { get; set; }
        //int Casing { get; set; }
        Script Script { get; set; }

        //SegmentChain<IGlyph> Composition { get; set; }
    }
}
