﻿using Epigrapher;
using System;
using System.Collections.Generic;

namespace Epigrapher.Lx
{
    /// <summary>
    /// A collection of Texts.
    /// </summary>
    public class Corpus : HashSet<Text>, IEntity
    {
        //a set of Texts
        public string Title { get; set; }
        public string Description { get; set; }

        public Guid Id => throw new NotImplementedException();

        public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public HashSet<Tag> Tags { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public List<string> Notes { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IEnumerable<IEntity> GetReferences()
        {
            throw new NotImplementedException();
        }
    }


}

