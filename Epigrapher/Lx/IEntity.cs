﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epigrapher
{
    public interface IEntity
    {
        Guid Id { get; }
        string Name { get; set; }
        string Description { get; set; }
        HashSet<Tag> Tags { get; set; }
        List<string> Notes { get; set; }

        IEnumerable<IEntity> GetReferences();
    }
}
