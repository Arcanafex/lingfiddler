﻿using System.Collections.ObjectModel;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public interface IManager<T>
    {
        string Title { get; }
        ObservableCollection<T> Items { get; set; }
        ICommand MoveDownCommand { get; set; }
        ICommand MoveUpCommand { get; set; }
        ICommand CreateCommand { get; set; }
        ICommand DeleteCommand { get; set; }
    }

    public interface IManageableItem
    {
        string Name { get; set; }
        string Description { get; set; }
        void Delete();
    }
}
