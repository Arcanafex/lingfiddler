﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EpigrapherWorkshop
{
    public class CodexViewModel : NotifyPropertyChangedBase
    {
        private readonly Codex m_Codex;

        public string Name
        {
            get => m_Codex.Name;
            set
            {
                m_Codex.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Description
        {
            get => m_Codex.Description;
            set
            {
                m_Codex.Description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public DataGrid DataGrid { get; set; }
        public ObservableCollection<CodexItemViewModel> Items { get; set; }

        public CodexViewModel(Codex codex)
        {
            this.m_Codex = codex;
            Items = new ObservableCollection<CodexItemViewModel>();
            //LoadItems();
            foreach (var segment in m_Codex)
            {
                Items.Add(new CodexItemViewModel(segment));
            }
        }

        public void LoadItems()
        {
            Items.Clear();

            var segmentsById = m_Codex.ToLookup(segment => segment.Id);

            foreach (var IdSet in segmentsById)
            {
                //Items.Add(new CodexItemViewModel(IdSet.ToArray()));
            }

            OnPropertyChanged(nameof(Items));
        }

        public void Add(Segment segment)
        {
            m_Codex.Add(segment);

            Items.Add(new CodexItemViewModel(segment));
        }

        public void Remove(Segment segment)
        {
            m_Codex.Remove(segment);

            Items.Remove(Items.FirstOrDefault(item => item.Id == segment.Id && item.Value == segment.Value));
        }

        public class CodexItemViewModel : NotifyPropertyChangedBase
        {
            private readonly Segment m_Segment;

            public Guid Id { get => m_Segment.Id; set => m_Segment.Id = value; }
            public string Name { get => m_Segment.Name; set => m_Segment.Name = value; }
            public string Value { get => m_Segment.Value; }
            public Dictionary<string, string> Layers { get; set; } = new Dictionary<string, string>();
            public string TestProperty { get; set; } = "Chonk";

            public CodexItemViewModel(Segment segment)
            {
                m_Segment = segment;

                foreach(var layer in m_Segment.Layers)
                {
                    Layers.Add(layer.Key.Name, layer.Value.Graph);
                }
            }
        }
    }
}
