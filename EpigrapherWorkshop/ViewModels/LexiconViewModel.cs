﻿using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace EpigrapherWorkshop
{
    public class LexiconViewModel : NotifyPropertyChangedBase
    {
        private readonly Lexicon m_Lexicon;

        public string Name
        {
            get => m_Lexicon.Name;
            set
            {
                m_Lexicon.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Description
        {
            get => m_Lexicon.Description;
            set
            {
                m_Lexicon.Description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public ObservableCollection<LexiconItemViewModel> Items { get; set; }

        public LexiconViewModel(Lexicon lexicon)
        {
            m_Lexicon = lexicon;
            Items = new ObservableCollection<LexiconItemViewModel>();

            foreach (var segmentValue in m_Lexicon)
            {
                Items.Add(new LexiconItemViewModel(segmentValue));
            }
        }

        public class LexiconItemViewModel : NotifyPropertyChangedBase
        {
            private readonly SegmentValue m_SegmentValue;

            public string Graph { get => m_SegmentValue.Graph; }
            public string Type { get => m_SegmentValue.Type.ToString(); }

            public LexiconItemViewModel(SegmentValue segmentValue)
            {
                m_SegmentValue = segmentValue;
            }
        }

    }
}