﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public class CodexManagerViewModel : ManagerViewModel<CodexManagerViewModel.CodexViewModel>
    {
        public override string ItemName => "Codex";
        protected override void PopulateCollection()
        {
            foreach (var codexName in Corpus.Current.CodexNames)
            {
                var codex = Corpus.Current.GetCodex(codexName);

                if (codex != null)
                {
                    Items.Add(new CodexViewModel(codex));
                }
            }
        }

        protected override void Create(object item)
        {
            var popup = new PopupDialog("Define New Codex", $"Codex {Corpus.Current.CodexCount + 1}");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineCodex(popup.TextContent);

                var codex = Corpus.Current.GetCodex(popup.TextContent);
                Items.Add(new CodexViewModel(codex));
            }

            popup.Close();
        }

        protected override void Delete(object item)
        {
            var codex = item as CodexViewModel;
            codex.Delete();
            Items.Remove(codex);
        }

        protected override void MoveUp(object item)
        {
            var codex = item as CodexViewModel;
            var index = Items.IndexOf(codex);

            if (index > 0)
            {
                Items.Move(index, index - 1);
            }

            Corpus.Current.SetCodexOrder(Items.Select(i => i.Name).ToList());
        }

        protected override void MoveDown(object item)
        {
            var codex = item as CodexViewModel;
            var index = Items.IndexOf(codex);

            if (index + 1 < Items.Count)
            {
                Items.Move(index, index + 1);
            }

            Corpus.Current.SetCodexOrder(Items.Select(i => i.Name).ToList());
        }

        public class CodexViewModel : NotifyPropertyChangedBase, IManageableItem
        {
            private readonly Codex m_Codex;

            public string Name { get => m_Codex.Name; set => m_Codex.Name = value; }
            public string Description { get => m_Codex.Description; set => m_Codex.Description = value; }

            public CodexViewModel(Codex codex)
            {
                m_Codex = codex;
            }

            public void Delete()
            {
                Corpus.Current.RemoveCodex(m_Codex);
            }
        }
    }
}
