﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public class TextDisplayViewModel : NotifyPropertyChangedBase
    {
        public SegmentViewModel DisplayedSegment => DisplayHistory[displayIndex];
        public string SegmentName
        {
            get => DisplayedSegment.SegmentName;
            set
            {
                DisplayedSegment.SegmentName = value;
                OnPropertyChanged(nameof(SegmentName));
            }
        }

        #region Navigation
        private List<SegmentViewModel> DisplayHistory { get; set; }
        private int displayIndex { get; set; }
        public bool ForwardAvailable => DisplayHistory.Count - 1 > displayIndex;
        public bool BackAvailable => displayIndex > 0;
        #endregion

        #region Commands
        public ICommand CommandNewText { get; set; }
        public ICommand CommandTestText { get; set; }
        public ICommand CommandForward { get; set; }
        public ICommand CommandBack { get; set; }
        public ICommand CommandAppendNewSegment { get; set; }
        public ICommand AddSegmentToCodexCommand { get; set; }
        #endregion

        public static HashSet<SegmentViewModel> SelectedSegments { get; set; }
        public bool ShowLayerNames { get => DisplayedSegment.ShowLayerNames; set => DisplayedSegment.ShowLayerNames = value; }


        public TextDisplayViewModel()
        {
            DisplayHistory = new List<SegmentViewModel>();
            SelectedSegments = new HashSet<SegmentViewModel>();

            CommandNewText = new CommandDelegate(NewText);
            //CommandTestText = new CommandDelegate(NewExpressionChain);
            CommandTestText = new CommandDelegate(NewTextSegment);
            CommandForward = new CommandDelegate(DisplayForward);
            CommandBack = new CommandDelegate(DisplayBack);
            CommandAppendNewSegment = new CommandDelegate(AppendSegment);
            AddSegmentToCodexCommand = new CommandDelegate(AddSegmentToCodex);

            DisplaySegment(Segment.Create("INTENTIONALLY BLANK"));
            //Test();
            SignalBus.RegisterListener<DisplaySegmentSignal>(HandleDisplaySignal);
        }

        private void Test()
        {
            //DisplaySegment(Segment.Create());
            //DisplaySegment(Tools.GenerateFacadeSegment(5));
            var segment = Segment.Create(Tools.ExampleExpression);
            Corpus.Current.DefineLayer("Composition");
            segment[Corpus.Current.GetLayer("Composition")] = Tools.ExampleWords;

            Corpus.Current.DefineLayer("Note");
            segment[Corpus.Current.GetLayer("Note")] = "An inciteful note!";

            DisplaySegment(segment);
        }

        private void RefreshNavigationState()
        {
            OnPropertyChanged(nameof(DisplayedSegment));
            OnPropertyChanged(nameof(ShowLayerNames));
            OnPropertyChanged(nameof(ForwardAvailable));
            OnPropertyChanged(nameof(BackAvailable));
        }

        private void HandleDisplaySignal(DisplaySegmentSignal signal)
        {
            DisplaySegment(signal.Segment);
        }

        public void DisplaySegment(Segment segment)
        {
            var view = new SegmentViewModel(segment);

            if (displayIndex + 1 >= DisplayHistory.Count)
            {
                DisplayHistory.Add(view);
                displayIndex = DisplayHistory.Count - 1;
            }
            else
                DisplayHistory.Insert(++displayIndex, view);

            while (DisplayHistory.Count > displayIndex + 1)
                DisplayHistory.RemoveAt(DisplayHistory.Count - 1);

            RefreshNavigationState();
        }

        private void DisplayForward(object obj)
        {
            displayIndex = Math.Min(displayIndex + 1, DisplayHistory.Count - 1);
            RefreshNavigationState();
        }

        private void DisplayBack(object obj)
        {
            displayIndex = Math.Max(0, displayIndex - 1);
            RefreshNavigationState();
        }

        public void NewBlankSegment()
        {
        }

        private void NewText(object obj)
        {
            var text = Segment.Create();

            DisplaySegment(text);
            DisplayedSegment.Select(true);
        }

        private void AppendSegment(object obj)
        {
            var segment = obj as Segment;

            if (segment == null)
                segment = Segment.Create();

            DisplayedSegment.Append(segment);
            DisplayedSegment.Refresh();
        }

        private void NewExpressionChain(object obj)
        {
            var expression = Tools.GenerateTestExpressionChain();
            //var text = Segment.Create();
            //text.Tokenize(expression);
            //var texts = Corpus.Current.GetCodex("Texts");
            //texts.Add(text);

            DisplaySegment(expression);
        }

        private void NewTextSegment(object obj)
        {
            var text = Segment.Create(Tools.ExampleText);
            DisplaySegment(text);
        }

        private void AddSegmentToCodex(object obj)
        {
            //var segmentViewModel = obj as SegmentViewModel;

            //if (segmentViewModel == null)
            //    throw new NullReferenceException();

            //segmentViewModel.AddToCodex(null);

            DisplayedSegment.AddToCodex(null);
        }

    }
}
