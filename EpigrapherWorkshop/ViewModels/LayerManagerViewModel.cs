﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public class LayerManagerViewModel : ManagerViewModel<LayerManagerViewModel.LayerViewModel>
    {
        public override string ItemName => "Layer";
        protected override void PopulateCollection()
        {
            foreach (var layerName in Corpus.Current.LayerNames)
            {
                var layer = Corpus.Current.GetLayer(layerName);

                if (layer != null)
                {
                    Items.Add(new LayerViewModel(layer));
                }
            }
        }

        protected override void Create(object item)
        {
            var popup = new PopupDialog($"Define New {ItemName}", $"{ItemName} {Corpus.Current.LayerCount + 1}");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineLayer(popup.TextContent);

                var layer = Corpus.Current.GetLayer(popup.TextContent);
                Items.Add(new LayerViewModel(layer));
            }

            popup.Close();
        }

        protected override void Delete(object item)
        {
            var layer = item as LayerViewModel;
            layer.Delete();
            Items.Remove(layer);
        }

        protected override void MoveUp(object item)
        {
            var layer = item as LayerViewModel;
            var index = Items.IndexOf(layer);

            if (index > 0)
            {
                Items.Move(index, index - 1);
            }

            Corpus.Current.SetLayerOrder(Items.Select(i => i.Name).ToList());
        }

        protected override void MoveDown(object item)
        {
            var layer = item as LayerViewModel;
            var index = Items.IndexOf(layer);

            if (index + 1 < Items.Count)
            {
                Items.Move(index, index + 1);
            }

            Corpus.Current.SetLayerOrder(Items.Select(i => i.Name).ToList());
        }

        public class LayerViewModel : NotifyPropertyChangedBase, IManageableItem
        {
            private readonly Layer m_Layer;

            public string Name { get => m_Layer.Name; set => m_Layer.Name = value; }
            public string Description { get => m_Layer.Description; set => m_Layer.Description = value; }

            public LayerViewModel(Layer layer)
            {
                m_Layer = layer;
            }

            public void Delete()
            {
                Corpus.Current.RemoveLayer(m_Layer);
            }
        }
    }
}
