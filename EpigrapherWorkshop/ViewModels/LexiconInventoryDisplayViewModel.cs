﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public class LexiconInventoryDisplayViewModel : NotifyPropertyChangedBase
    {
        public LexiconViewModel LexiconViewModel { get; set; }

        public ICommand CommandAddLexicon { get; set; }
        public ICommand CommandEditLexicon { get; set; }

        public List<string> LexiconNames { get; set; }
        private string selectedLexicon;
        public string SelectedLexicon
        {
            get => selectedLexicon;
            set
            {
                selectedLexicon = value;

                if (!string.IsNullOrEmpty(selectedLexicon) && LexiconViewModel?.Name != selectedLexicon)
                {
                    LexiconViewModel = new LexiconViewModel(Corpus.Current.GetLexicon(selectedLexicon));
                    OnPropertyChanged(nameof(LexiconViewModel));
                }

                OnPropertyChanged(nameof(SelectedLexicon));
            }
        }

        public LexiconInventoryDisplayViewModel()
        {
            CommandAddLexicon = new CommandDelegate(AddLexicon);
            CommandEditLexicon = new CommandDelegate(EditLexicon);

            selectedLexicon = Corpus.Current.LexiconNames.FirstOrDefault();
            RefreshDisplay();
        }

        public void RefreshDisplay()
        {
            if (Corpus.Current == null)
                return;

            LexiconNames = Corpus.Current.LexiconNames;
            OnPropertyChanged(nameof(LexiconNames));
            SelectedLexicon = selectedLexicon;
        }

        private void AddLexicon(object obj)
        {
            var popup = new PopupDialog("Add Lexicon", "New Lexicon");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineLexicon(popup.TextContent);
                RefreshDisplay();
            }

            popup.Close();
        }

        private void EditLexicon(object obj)
        {
            var popup = new PopupDialog("Edit Lexicon", SelectedLexicon);
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                LexiconViewModel.Name = popup.TextContent;
                RefreshDisplay();

                SelectedLexicon = LexiconViewModel.Name;
            }

            popup.Close();
        }
    }
}
