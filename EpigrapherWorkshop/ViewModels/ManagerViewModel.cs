﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public abstract class ManagerViewModel<T> : NotifyPropertyChangedBase, IManager<T>
    {
        public string Title => $"{ItemName} Manager";
        public virtual string ItemName => "Item";
        public string CreateItemMenuHeader => $"Create New {ItemName}";

        public ObservableCollection<T> Items { get; set; }
        public ICommand CreateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand MoveUpCommand { get; set; }
        public ICommand MoveDownCommand { get; set; }

        public ManagerViewModel()
        {
            Items = new ObservableCollection<T>();

            CreateCommand = new CommandDelegate(Create);
            DeleteCommand = new CommandDelegate(Delete);
            MoveUpCommand = new CommandDelegate(MoveUp);
            MoveDownCommand = new CommandDelegate(MoveDown);

            PopulateCollection();
        }

        protected abstract void PopulateCollection();
        protected abstract void Create(object item);
        protected abstract void Delete(object item);
        protected abstract void MoveUp(object item);
        protected abstract void MoveDown(object item);
    }
}
