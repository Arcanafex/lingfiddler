﻿using System.Linq;

namespace EpigrapherWorkshop
{
    public class LexiconManagerViewModel : ManagerViewModel<LexiconManagerViewModel.LexiconViewModel>
    {
        public override string ItemName => "Lexicon";
        protected override void PopulateCollection()
        {
            foreach (var lexiconName in Corpus.Current.LexiconNames)
            {
                var lexicon = Corpus.Current.GetLexicon(lexiconName);

                if (lexicon != null)
                {
                    Items.Add(new LexiconViewModel(lexicon));
                }
            }
        }

        protected override void Create(object item)
        {
            var popup = new PopupDialog("Define New Lexicon", $"Lexicon {Corpus.Current.LexiconCount + 1}");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineLexicon(popup.TextContent);

                var lexicon = Corpus.Current.GetLexicon(popup.TextContent);
                Items.Add(new LexiconViewModel(lexicon));
            }

            popup.Close();
        }

        protected override void Delete(object item)
        {
            var lexicon = item as LexiconViewModel;
            lexicon.Delete();
            Items.Remove(lexicon);
        }

        protected override void MoveUp(object item)
        {
            var lexicon = item as LexiconViewModel;
            var index = Items.IndexOf(lexicon);

            if (index > 0)
            {
                Items.Move(index, index - 1);
            }

            Corpus.Current.SetLexiconOrder(Items.Select(i => i.Name).ToList());
        }

        protected override void MoveDown(object item)
        {
            var lexicon = item as LexiconViewModel;
            var index = Items.IndexOf(lexicon);

            if (index + 1 < Items.Count)
            {
                Items.Move(index, index + 1);
            }

            Corpus.Current.SetLexiconOrder(Items.Select(i => i.Name).ToList());
        }

        public class LexiconViewModel : NotifyPropertyChangedBase, IManageableItem
        {
            private readonly Lexicon m_Lexicon;

            public string Name { get => m_Lexicon.Name; set => m_Lexicon.Name = value; }
            public string Description { get => m_Lexicon.Description; set => m_Lexicon.Description = value; }

            public LexiconViewModel(Lexicon lexicon)
            {
                m_Lexicon = lexicon;
            }

            public void Delete()
            {
                Corpus.Current.RemoveLexicon(m_Lexicon);
            }
        }
    }
}
