﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public class CodexInventoryDisplayViewModel : NotifyPropertyChangedBase
    {
        public CodexViewModel CodexViewModel { get; set; }

        #region Commands
        public ICommand AddCodexCommand { get; set; }
        public ICommand EditCodexCommand { get; set; }
        public ICommand DisplayItemCommand { get; set; }
        public ICommand RemoveItemCommand { get; set; }
        #endregion

        public List<string> CodexNames { get; set; }

        private string selectedCodex;
        public string SelectedCodex
        {
            get => selectedCodex;
            set
            {
                selectedCodex = value;

                if (!string.IsNullOrEmpty(selectedCodex) && CodexViewModel?.Name != selectedCodex)
                {
                    CodexViewModel = new CodexViewModel(Corpus.Current.GetCodex(selectedCodex));
                    OnPropertyChanged(nameof(CodexViewModel));
                }

                OnPropertyChanged(nameof(SelectedCodex));
            }
        }

        public CodexInventoryDisplayViewModel()
        {
            AddCodexCommand = new CommandDelegate(AddCodex);
            EditCodexCommand = new CommandDelegate(EditCodex);
            DisplayItemCommand = new CommandDelegate(DisplayItem);
            RemoveItemCommand = new CommandDelegate(RemoveItem);

            SignalBus.RegisterListener<AddSegmentToCodexSignal>(HandleAddSegmentToCodexSignal);

            selectedCodex = Corpus.Current.CodexNames.FirstOrDefault();

            RefreshDisplay();
        }

        public void RefreshDisplay()
        {
            CodexNames = Corpus.Current.CodexNames;
            OnPropertyChanged(nameof(CodexNames));

            SelectedCodex = selectedCodex;
        }

        private void AddCodex(object obj)
        {
            var popup = new PopupDialog("Add Codex", "New Codex");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineCodex(popup.TextContent);
                RefreshDisplay();
            }

            popup.Close();
        }

        private void EditCodex(object obj)
        {
            var popup = new PopupDialog("Edit Codex", SelectedCodex);
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                CodexViewModel.Name = popup.TextContent;
                RefreshDisplay();

                SelectedCodex = CodexViewModel.Name;
            }

            popup.Close();
        }

        private void DisplayItem(object obj)
        {
            var segment = obj as Segment;

            if (segment == null)
                throw new NullReferenceException();

            SignalBus.Fire(new DisplaySegmentSignal(segment));
        }

        private void RemoveItem(object obj)
        {
            var segment = obj as Segment;

            if (segment == null)
                throw new NullReferenceException();

            CodexViewModel.Remove(segment);
        }

        private void HandleAddSegmentToCodexSignal(AddSegmentToCodexSignal signal)
        {
            if (signal?.Segment == null)
                throw new NullReferenceException();

            if (signal.Codex == null)
            {
                CodexViewModel.Add(signal.Segment);
            }
        }
    }
}
