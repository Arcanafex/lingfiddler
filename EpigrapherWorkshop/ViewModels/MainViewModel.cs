﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public class MainViewModel : NotifyPropertyChangedBase
    {
        private Corpus m_corpus;

        public string OpenCorpusName => m_corpus.Name;
        public string WindowTitle => $"Epigrapher - {OpenCorpusName}";

        public ICommand RenameCorpusCommand { get; set; }
        
        public ICommand DefineLayerCommand { get; set; }
        public ICommand ManageLayersCommand { get; set; }

        public ICommand DefineCodexCommand { get; set; }
        public ICommand ManageCodexesCommand { get; set; }

        public ICommand DefineLexiconCommand { get; set; }
        public ICommand ManageLexiconsCommand { get; set; }

        public ICommand SaveCorpusCommand { get; set; }
        public ICommand LoadCorpusCommand { get; set; }


        public MainViewModel()
        {
            NewCorpus();
            RenameCorpusCommand = new CommandDelegate(RenameCorpus);

            DefineLayerCommand = new CommandDelegate(DefineLayer);
            ManageLayersCommand = new CommandDelegate(ManageLayers);

            DefineCodexCommand = new CommandDelegate(DefineCodex);
            ManageCodexesCommand = new CommandDelegate(ManageCodexes);

            DefineLexiconCommand = new CommandDelegate(DefineLexicon);
            ManageLexiconsCommand = new CommandDelegate(ManageLexicon);

            SaveCorpusCommand = new CommandDelegate(SaveCorpus);
            LoadCorpusCommand = new CommandDelegate(LoadCorpus);

            FillCorpus();
        }

        public void NewCorpus()
        {
            m_corpus = Corpus.Create("Unnamed Corpus");
            Corpus.SetCurrentCorpus(m_corpus);
        }

        public void RenameCorpus(object obj)
        {
            var popup = new PopupDialog("Rename Corpus", Corpus.Current.Name);
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.Name = popup.TextContent;
            }

            popup.Close();

            OnPropertyChanged(nameof(WindowTitle));            
        }

        private void DefineLayer(object obj)
        {
            var popup = new PopupDialog("Define New Layer", $"Layer {Corpus.Current.LayerCount + 1}");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineLayer(popup.TextContent);
            }

            popup.Close();
        }

        private void ManageLayers(object obj)
        {
            var manager = new PopupManager();
            manager.DataContext = new LayerManagerViewModel();
            manager.ShowDialog();
        }

        private void DefineCodex(object obj)
        {
            var popup = new PopupDialog("Define New Codex", $"Codex {Corpus.Current.CodexCount + 1}");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineCodex(popup.TextContent);
            }

            popup.Close();
        }

        private void ManageCodexes(object obj)
        {
            var manager = new PopupManager();
            manager.DataContext = new CodexManagerViewModel();
            manager.ShowDialog();
        }

        private void DefineLexicon(object obj)
        {
            var popup = new PopupDialog("Define New Lexicon", $"Lexicon {Corpus.Current.LexiconCount + 1}");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.DefineCodex(popup.TextContent);
            }

            popup.Close();
        }

        private void ManageLexicon(object obj)
        {
            var manager = new PopupManager();
            manager.DataContext = new LexiconManagerViewModel();
            manager.ShowDialog();
        }

        private void FillCorpus()
        {
            m_corpus.DefineCodex("Cheeses");
            m_corpus.DefineCodex("Waffles");
            m_corpus.DefineCodex("Garbage");

            m_corpus.DefineLexicon("Bubbles");
            m_corpus.DefineLexicon("puppies");
            m_corpus.DefineLexicon("Glyphs");

            m_corpus.DefineLayer("Main");

            var currentId = Guid.NewGuid();

            foreach(var name in m_corpus.CodexNames)
            {
                var codex = m_corpus.GetCodex(name);

                for(int i = 0; i < 20; i++)
                {
                    if (i % 4 == 0)
                        currentId = Guid.NewGuid();

                    var segment = Segment.Create();
                    segment.Id = currentId;
                    segment.Value = $"{name}";
                    codex.Add(segment);
                }
            }

            foreach(var name in m_corpus.LexiconNames)
            {
                var lexicon = m_corpus.GetLexicon(name);

                for (int i = 50; i < 128; i++)
                {
                    var value = new SegmentValue($"{(char)i}");
                    lexicon.Add(value);
                }
            }
        }

        private void SaveCorpus(object obj)
        {
            var popup = new PopupDialog("Save Corpus", @"C:\Test.bin");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Current.Save(popup.TextContent);
            }

            popup.Close();
        }

        private void LoadCorpus(object obj)
        {
            var popup = new PopupDialog("Save Corpus", @"C:\Test.bin");
            var result = popup.ShowDialog();

            if (result.HasValue && result.Value)
            {
                Corpus.Load(popup.TextContent);
            }

            popup.Close();
        }
    }
}
