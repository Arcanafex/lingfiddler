﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    public class LayerSegmentValueViewModel : NotifyPropertyChangedBase
    {
        public enum DisplayMode
        {
            GraphDisplay,
            GraphEdit,
            CompositionDisplay,
            Image
        }

        private Segment m_segment;
        private Layer m_layer;
        public SegmentValue Value { get; set; }
        public string LayerName => m_layer.Name;

        private DisplayMode mode;
        public DisplayMode Mode
        {
            get => mode;
            set
            {
                mode = value;
                OnPropertyChanged(nameof(Mode));
            }
        }

        public List<string> LayerOptions
        {
            get => Corpus.Current.LayerNames;
        }

        public int LayerIndex
        {
            get => LayerOptions.IndexOf(m_layer.Name);
            set
            {
                var layer = Corpus.Current.GetLayer(LayerOptions[value]);

                if (m_layer != layer)
                {
                    m_layer = layer;
                    OnPropertyChanged(nameof(LayerName));
                }
            }
        }

        #region Graph Rendering
        public string Graph
        {
            get => Value.Graph;
            set
            {
                Value.Graph = value;
                OnPropertyChanged(nameof(Graph));
            }
        }
        #endregion

        #region Composition Rendering
        public ObservableCollection<SegmentViewModel> Composition { get; set; }
        #endregion

        public ICommand ChangeLayer { get; set; }
        public ICommand SelectLayerValue { get; set; }

        private bool editMode;
        public bool EditMode
        {
            get => editMode;
            set
            {
                editMode = value;
                OnPropertyChanged(nameof(EditMode));
            }
        }
        //public bool ReadMode => !(Value.Type == SegmentValue.ValueType.Text && EditMode);

        private bool changingLayerName;
        public bool ChangeLayerName
        {
            get => changingLayerName;
            set
            {
                changingLayerName = value;
                OnPropertyChanged(nameof(ChangeLayerName));
            }
        }


        public LayerSegmentValueViewModel(Segment segment, Layer layer)
        {
            // TODO: explicitly handle case of value being null
            m_segment = segment;
            m_layer = layer;
            Value = segment[layer] ?? SegmentValue.Empty;

            ChangeLayerName = false;

            ChangeLayer = new CommandDelegate(CommandChangeLayer);
            SelectLayerValue = new CommandDelegate(CommandSelectLayerValue);

            switch(Value.Type)
            {
                default:
                case SegmentValue.ValueType.Text:
                    Mode = DisplayMode.GraphEdit;
                    break;
                case SegmentValue.ValueType.Chain:
                    Mode = DisplayMode.CompositionDisplay;
                    AnalyzeComposition();
                    break;
            }

        }

        public void AnalyzeComposition()
        {
            if (Composition == null)
            {
                Composition = new ObservableCollection<SegmentViewModel>();
            }

            if (Value.Composition != null)
            {
                foreach (var segment in Value.Composition)
                {
                    Composition.Add(new SegmentViewModel(segment));
                }
            }

            OnPropertyChanged(nameof(Composition));
        }

        private void CommandChangeLayer(object param)
        {
            if (!ChangeLayerName)
            {
                ChangeLayerName = true;
                OnPropertyChanged(nameof(LayerOptions));
            }
            else
                ChangeLayerName = false;
        }

        private void CommandSelectLayerValue(object param)
        {
            EditMode = !EditMode;

            //OnPropertyChanged(nameof(ReadMode));
            OnPropertyChanged(nameof(Graph));
        }
    }
}
