﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;


namespace EpigrapherWorkshop
{
    public class SegmentViewModel : NotifyPropertyChangedBase
    {
        private readonly Segment m_Segment;
        private bool m_ShowLayerNames;

        public string SegmentName
        {
            get => m_Segment.Name;
            set
            {
                m_Segment.Name = value;
                OnPropertyChanged(nameof(SegmentName));
            }
        }
        public bool ShowLayerNames
        {
            get => m_ShowLayerNames;
            set
            {
                m_ShowLayerNames = value;
                OnPropertyChanged(nameof(ShowLayerNames));

                foreach(var layer in Layers)
                {
                    layer.ChangeLayerName = false;
                }
            }
        }
        public ObservableCollection<LayerSegmentValueViewModel> Layers { get; set; }

        public bool Selected => TextDisplayViewModel.SelectedSegments?.Contains(this) ?? false;

        public ICommand SelectSegmentCommand { get; set; }
        public ICommand CommitChangeCommand { get; set; }
        public ICommand CancelChangeCommand { get; set; }
        public ICommand DisplaySegmentCommand { get; set; }
        public ICommand AddLayerCommand { get; set; }

        public Guid Id => m_Segment.Id;

        public SegmentViewModel(Segment segment)
        {
            m_Segment = segment;
            Layers = new ObservableCollection<LayerSegmentValueViewModel>();

            foreach(var name in Corpus.Current.LayerNames)
            {
                var layer = Corpus.Current.GetLayer(name);

                if (segment[layer] != null)
                    Layers.Add(new LayerSegmentValueViewModel(segment, layer));
            }


            DisplaySegmentCommand = new CommandDelegate(DisplaySegment);
            SelectSegmentCommand = new CommandDelegate(SelectSegment);
            //CommandCommitChange = new CommandDelegate(CommitChange);
            AddLayerCommand = new CommandDelegate(LayerAvailable, AddLayer);
        }

        public void Refresh()
        {
            //Value = m_segment.Value;
        }

        public void Append(Segment segment)
        {
            //m_segment.Append(segment);
            Refresh();
        }

        public void Select(bool makeSelected = true)
        {
            if (makeSelected)
                TextDisplayViewModel.SelectedSegments.Add(this);
            else
                TextDisplayViewModel.SelectedSegments.Remove(this);
        }

        public void AddToCodex(string codexName)
        {
            var codex = Corpus.Current.GetCodex(codexName);

            SignalBus.Fire(new AddSegmentToCodexSignal(m_Segment, codex));
        }

        private void SelectSegment(object obj)
        {
            Select();

            //var popup = new PopupDialog();
            //popup.PopupTitle = "Selected Segment";
            //popup.TextContent = Graph;

            //var accepted = popup.ShowDialog();

            //if (accepted.HasValue && accepted.Value)
            //{

            //}

            //popup.Close();
        }

        private void DisplaySegment(object obj)
        {
            SignalBus.Fire(new DisplaySegmentSignal(m_Segment));
        }

        private void CommitChange(object obj)
        {
        }

        private bool LayerAvailable(object obj)
        {
            if (Corpus.Current.LayerNames.Count > m_Segment.Layers.Count)
            {
                return true;
            }
            else
            {
                var message = new PopupMessage(
                    "Notification", 
                    "There are no more layers available to add. Try defining some more."
                    );

                message.ShowDialog();
                return false;
            }
        }

        private void AddLayer(object obj)
        {
            foreach(var layer in Corpus.Current.LayerNames.Select(name => Corpus.Current.GetLayer(name)))
            {
                if (!m_Segment.Layers.ContainsKey(layer))
                {
                    m_Segment[layer] = SegmentValue.Empty;
                    Layers.Add(new LayerSegmentValueViewModel(m_Segment, layer));
                    OnPropertyChanged(nameof(Layers));
                    return;
                }
            }

        }
    }
}
