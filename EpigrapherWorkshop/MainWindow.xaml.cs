﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace EpigrapherWorkshop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();

            TextDisplayControl.DataContext = new TextDisplayViewModel();
            CodexInventoryDisplayControl.DataContext = new CodexInventoryDisplayViewModel();
            LexiconInventoryDisplayControl.DataContext = new LexiconInventoryDisplayViewModel();

            var task = UpdateMousPos();
        }

        private async Task UpdateMousPos()
        {
            while (true)
            {
                await Task.Delay(50);
                var point = Mouse.GetPosition(this);

                PositionX.Text = $"{point.X:0.0000}";
                PositionY.Text = $"{point.Y:0.0000}";
            }
        }
    }
}
