﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace EpigrapherWorkshop
{
    [Serializable]
    public class Corpus
    {
        public static Corpus Current { get; private set; }
        public static void SetCurrentCorpus(Corpus corpus)
        {
            if (Current != corpus)
                Current = corpus;
        }

        public static Corpus Create(string name)
        {
            var corpus = new Corpus();
            corpus.Initialize(name);

            return corpus;
        }

        public Guid Id { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public void Initialize(string name = null)
        {
            Id = Guid.NewGuid();

            if (!string.IsNullOrEmpty(name))
                Name = name;

            DefineCodex("Texts");
            DefineLayer("Surface");
        }

        #region Layers
        private HashSet<Layer> m_layers { get; set; } = new HashSet<Layer>();
        private List<string> m_layerOrder = new List<string>();
        private Layer m_defaultLayer;

        #region Layer Public Accessors
        public int LayerCount => m_layers.Count;
        public List<string> LayerNames => m_layers.Select(layer => layer.Name).OrderBy(layer => m_layerOrder.IndexOf(layer)).ThenBy(layer => layer).ToList();
        public static Layer DefaultLayer
        {
            get
            {
                if (Current == null)
                    return Layer.DefaultLayer;

                if (Current.m_defaultLayer == null)
                {
                    var layerName = Current?.LayerNames?.FirstOrDefault();

                    if (string.IsNullOrEmpty(layerName))
                    {
                        layerName = Layer.DefaultLayerName;
                        Current?.DefineLayer(layerName);
                    }

                    Current.m_defaultLayer = Current?.GetLayer(layerName);
                }

                return Current.m_defaultLayer;
            }

            private set
            {
                if (Current == null)
                    return;

                Current.m_defaultLayer = value;
            }
        }
        #endregion

        #region Layer Management
        public void DefineLayer(string name)
        {
            if (!m_layers.Any(layer => layer.Name == name))
            {
                m_layers.Add(Layer.Create(name));
                m_layerOrder.Add(name);
            }
        }

        public Layer GetLayer(string name)
        {
            var layer = m_layers.FirstOrDefault(l => l.Name == name);

            return layer;
        }

        public void SetLayerOrder(List<string> orderedNames)
        {
            m_layerOrder = orderedNames;
        }

        public void SetDefaultLayer(Layer layer)
        {
            DefaultLayer = layer;
        }

        public bool RemoveLayer(string layerName)
        {
            var layer = GetLayer(layerName);
            return RemoveLayer(layer);
        }

        public bool RemoveLayer(Layer layer)
        {
            m_layerOrder.Remove(layer.Name);
            return m_layers.Remove(layer);
        }
        #endregion
        #endregion

        #region Codexes
        private HashSet<Codex> m_codexes { get; set; } = new HashSet<Codex>();
        private List<string> m_codexOrder = new List<string>();

        #region Codex Public Accessors
        public int CodexCount => m_codexes.Count;
        public List<string> CodexNames => m_codexes.Select(codex => codex.Name).OrderBy(codex => m_codexOrder.IndexOf(codex)).ThenBy(codex => codex).ToList();
        #endregion

        #region Codex Management
        public void DefineCodex(string name)
        {
            if (!m_codexes.Any(codex => codex.Name == name))
            {
                m_codexes.Add(Codex.Create(name));
                m_codexOrder.Add(name);
            }
        }

        public Codex GetCodex(string name)
        {
            var codex = m_codexes.FirstOrDefault(cdx => cdx.Name == name);

            return codex;
        }

        public void SetCodexOrder(List<string> orderedNames)
        {
            m_codexOrder = orderedNames;
        }

        public bool RemoveCodex(string codexName)
        {
            var codex = GetCodex(codexName);
            return RemoveCodex(codex);
        }

        public bool RemoveCodex(Codex codex)
        {
            m_codexOrder.Remove(codex.Name);
            return m_codexes.Remove(codex);
        }
        #endregion
        #endregion

        #region Lexicons
        private HashSet<Lexicon> m_lexicons { get; set; } = new HashSet<Lexicon>();
        private List<string> m_lexiconOrder = new List<string>();

        #region Lexicon Public Accessors
        public int LexiconCount => m_lexicons.Count;
        public List<string> LexiconNames => m_lexicons.Select(lexicon => lexicon.Name).OrderBy(lexicon => m_lexiconOrder.IndexOf(lexicon)).ThenBy(lexicon => lexicon).ToList();
        #endregion

        #region Lexicon Management
        public void DefineLexicon(string name)
        {
            if (!m_lexicons.Any(lexicon => lexicon.Name == name))
            {
                m_lexicons.Add(Lexicon.Create(name));
                m_lexiconOrder.Add(name);
            }
        }

        public Lexicon GetLexicon(string name)
        {
            var lexicon = m_lexicons.FirstOrDefault(lxcn => lxcn.Name == name);

            return lexicon;
        }

        public void SetLexiconOrder(List<string> orderedNames)
        {
            m_lexiconOrder = orderedNames;
        }

        public bool RemoveLexicon(string lexiconName)
        {
            var lexicon = GetLexicon(lexiconName);
            return RemoveLexicon(lexicon);
        }

        public bool RemoveLexicon(Lexicon lexicon)
        {
            m_lexiconOrder.Remove(lexicon.Name);
            return m_lexicons.Remove(lexicon);
        }

        #endregion

        #endregion

        public static void Load(string path)
        {
            // loads the Id from the saved file
            using (var filestream = File.OpenRead(path))
            {
                var formatter = new BinaryFormatter();
                var bin = formatter.Deserialize(filestream);
                SetCurrentCorpus(bin as Corpus);
            }
        }

        public void Save(string path)
        {
            using (var memoryStream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, this);

                using (var writer = new BinaryWriter(File.Open(path, System.IO.FileMode.CreateNew)))
                {
                    writer.Write(memoryStream.ToArray());
                }
            }
        }
    }
}
