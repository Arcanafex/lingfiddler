﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpigrapherWorkshop
{
    /// <summary>
    /// A hierarchical collection of Segment instances.
    /// </summary>
    public class Codex : ICollection<Segment>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public Codex Parent { get; set; }

        public int Count => Collection.Count();
        public bool IsReadOnly => ReadOnly;
        public bool ReadOnly { get; set; }

        private HashSet<Segment> Collection { get; set; }

        private Codex()
        {
            Collection = new HashSet<Segment>();
        }

        public static Codex Create(string name)
        {
            return new Codex() { Name = name };
        }

        public void Add(Segment item)
        {
            Collection.Add(item);
        }

        public void Clear()
        {
            Collection.Clear();
        }

        public bool Contains(Segment item)
        {
            return Collection.Contains(item);
        }

        public void CopyTo(Segment[] array, int arrayIndex)
        {
            var codex = Collection.ToArray();

            for(int i = 0; i < codex.Length && (i + arrayIndex) < array.Length; i++)
            {
                array[arrayIndex + i] = codex[i];
            }
        }

        public bool Remove(Segment item)
        {
            return Collection.Remove(item);
        }

        public IEnumerator<Segment> GetEnumerator()
        {
            return Collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Collection.GetEnumerator();
        }
    }
}
