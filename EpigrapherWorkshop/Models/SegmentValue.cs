﻿using System;
using System.Windows.Controls;

namespace EpigrapherWorkshop
{
    public class SegmentValue
    {
        public enum ValueType
        {
            /// <summary>
            /// Segment Chain
            /// </summary>
            Chain,
            /// <summary>
            /// string or equivalent
            /// </summary>
            Text,
            /// <summary>
            /// Vector graphic
            /// </summary>
            Graphic,
            /// <summary>
            /// Bitmap graphic
            /// </summary>
            Image,
            /// <summary>
            /// Sound wave
            /// </summary>
            Audio,
            /// <summary>
            /// Multi-dimensional vertex mesh
            /// </summary>
            Model
        }

        public Guid Id { get; set; }
        public object Token { get; set; }
        public ValueType Type { get; set; }


        // TODO: Add type checking and warnings for type/accessor access mismatches

        /// <summary>
        /// Chain value type data accessor.
        /// </summary>
        public SegmentChain Composition { get => Token as SegmentChain; set => Token = value; }
        /// <summary>
        /// Text value type data accessor.
        /// </summary>
        public string Graph { get => Token as string; set => Token = value; }
        /// <summary>
        /// Image or Graphic value type data accessor.
        /// </summary>
        public Image Image { get; set; }

        // TODO: Implement accessors for the following types
        // Audio value type data accessor.
        // Model value type data accessor.
        
        public SegmentValue(ValueType type)
        {
            Id = Guid.NewGuid();
            Type = type;
            Token = type.ToString();
        }

        /// <summary>
        /// Constructor for Text value type.
        /// </summary>
        /// <param name="value"></param>
        public SegmentValue(string value)
        {
            Id = Guid.NewGuid();
            Type = ValueType.Text;
            Token = value;
        }

        /// <summary>
        /// Condstructor for Chain value type of Text segments.
        /// </summary>
        /// <param name="values"></param>
        public SegmentValue(string[] values)
        {
            Id = Guid.NewGuid();
            Type = ValueType.Chain;
            Composition = SegmentChain.Create(values);
        }

        public override string ToString()
        {
            return Token.ToString();
        }

        public static implicit operator string(SegmentValue value) => value?.ToString();
        public static implicit operator SegmentValue(string value) => new SegmentValue(value);
        public static implicit operator SegmentValue(string[] values) => new SegmentValue(values);

        public static SegmentValue Empty => new SegmentValue(string.Empty);
    }
}
