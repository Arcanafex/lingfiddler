﻿namespace EpigrapherWorkshop
{
    /// <summary>
    /// Default data type for this layer
    /// </summary>
    public enum LayerType
    {
        Text,
        Graphic,
        Image,
        Audio,
        Model
    }


    public class Layer
    {
        public static string DefaultLayerName => "Surface";
        public static Layer DefaultLayer => Create(DefaultLayerName);

        public string Name { get; set; }
        public string Description { get; set; }
        public LayerType Type { get; set; }

        private Layer() { }

        public static Layer Create(string name)
        {
            return new Layer() { Name = name };
        }

        public override bool Equals(object obj)
        {
            var layer = obj as Layer;
            return layer?.Name == Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
