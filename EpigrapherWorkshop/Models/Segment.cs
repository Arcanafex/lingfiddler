﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace EpigrapherWorkshop
{
    public class Segment
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Dictionary<Layer, SegmentValue> Layers { get; private set; }

        public SegmentValue Value
        {
            get
            {
                Layers.TryGetValue(Corpus.DefaultLayer, out SegmentValue value);

                return value;
            }
            set
            {
                if (Layers.ContainsKey(Corpus.DefaultLayer))
                {
                    Layers[Corpus.DefaultLayer] = value;
                }
                else
                {
                    Layers.Add(Corpus.DefaultLayer, value);
                }
            }
        }

        public SegmentValue this[Layer layer]
        {
            get
            {
                if (layer == null)
                    layer = Corpus.DefaultLayer;

                Layers.TryGetValue(layer, out SegmentValue value);

                return value;
            }
            set
            {
                if (layer == null)
                    layer = Corpus.DefaultLayer;

                if (Layers.ContainsKey(layer))
                    Layers[layer] = value;
                else
                    Layers.Add(layer, value);
            }
        }

        //public HashSet<Segment> Links { get; set; }

        private Segment()
        {
            Id = Guid.NewGuid();
            Layers = new Dictionary<Layer, SegmentValue>();
        }

        public static Segment Create(string value = "")
        {
            var segment = new Segment
            {
                Value = value
            };

            return segment;
        }

        public static Segment Create(string[] values)
        {
            var segment = new Segment
            {
                Value = values
            };

            return segment;
        }

        public static Segment Create(string value, Layer layer)
        {
            if (layer == null)
                return Create(value);

            var segment = new Segment();
            segment[layer] = value;

            return segment;
        }

        public static Segment Create(string[] values, Layer layer)
        {
            if (layer == null)
                return Create(values);

            var segment = new Segment();
            segment[layer] = values;

            return segment;
        }

        ///// <summary>
        ///// Turns this segment into a token reference of another segment.
        ///// </summary>
        ///// <param name="tokenOf">The segment this one should be considered an instance of.</param>
        public void Tokenize(Segment tokenOf)
        {
            Id = tokenOf.Id;
        }

        //public enum AnalysisOptions
        //{
        //    WhiteSpace,
        //    Characters,
        //    Delimiter,
        //    Pattern
        //}

        /// <summary>
        /// Analyzes this segment into a constituent segment chain. Layer defaults to the DefaultLayer of the current corpus if not specified.
        /// </summary>
        /// <returns>The reference to the SegmentValue with the new SegmentChain as its composition</returns>
        public SegmentValue Analyze(Layer layer = null, string splitOn = null, StringSplitOptions splitOptions = StringSplitOptions.None)
         {
            var segmentValue = this[layer];

            if (!(segmentValue.Composition?.Count > 0))
            {
                if (string.IsNullOrEmpty(splitOn))
                    segmentValue.Composition = SegmentChain.Create(segmentValue.Graph);
                else
                    segmentValue.Composition = SegmentChain.Create(segmentValue.Graph.Split(new string[] { splitOn }, splitOptions));
            }
            else
            {
                // composition already exists
            }

            return segmentValue;
        }

        public SegmentValue Analyze(Layer layer, char[] splitOn, StringSplitOptions splitOptions = StringSplitOptions.None)
        {
            var segmentValue = this[layer];

            if (!(segmentValue.Composition?.Count > 0))
            {
                if (splitOn == null || splitOn.Length == 0)
                    segmentValue.Composition = SegmentChain.Create(segmentValue.Graph.ToCharArray().Select(c => c.ToString()).ToArray());
                else
                    segmentValue.Composition = SegmentChain.Create(segmentValue.Graph.Split(splitOn, splitOptions));
            }
            else
            {
                // composition already exists
            }

            return segmentValue;
        }

        public SegmentValue Analyze(Layer layer, Regex pattern)
        {
            var segmentValue = this[layer];

            if (!(segmentValue.Composition?.Count > 0))
            {
                var elements = new List<string>();

                foreach (Match match in pattern.Matches(segmentValue.Graph))
                {
                    elements.Add(match.Value);
                }

                segmentValue.Composition = SegmentChain.Create(elements.ToArray());
            }

            return segmentValue;
        }

        //public void Link(Segment segment)
        //{
        //    Links.Add(segment);
        //    segment.Links.Add(this);
        //}

        //public void Unlink(Segment segment)
        //{
        //    Links.Remove(segment);
        //    segment.Links.Remove(this);
        //}

        /// <summary>
        /// Splits this segment in two.
        /// </summary>
        /// <param name="offset">The index of the point in the Value string at which the segment is to be split.</param>
        /// <returns>The new, second segment.</returns>
        public Segment Split(int offset)
        {
            var splitSegment = Create(Value.Graph.Substring(offset));

            if (Value.Composition != null)
            {
                if (offset < Value.Composition.Count)
                {

                }
            }

            //var splitSegment = Create(Value.Substring(offset));
            //splitSegment.Layer = Layer;
            //Value = Value.Substring(0, offset);

            //this.InsertAfter(splitSegment);

            return splitSegment;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        //public List<string> GetSegmentGraph()
        //{
        //    //if (IsToken)
        //    //    return GraphSegment(Token);
        //    //else
        //        return string.IsNullOrEmpty(Value) ? new List<string>() : new List<string>() { Value };
        //}

        //public static List<string> GraphSegment(Segment currentSegment)
        //{
        //    var graph = new List<string>();
        //    var stack = new Stack<Segment>();

        //    //bool done = false;

        //    //while (!done)
        //    //{
        //    //    if (currentSegment == null)
        //    //    {
        //    //        if (stack.Count > 0)
        //    //            currentSegment = stack.Pop();
        //    //    }
        //    //    else if (currentSegment.IsToken)
        //    //    {
        //    //        stack.Push(currentSegment.NextSegment);
        //    //        currentSegment = currentSegment.Token;
        //    //    }
        //    //    else
        //    //    {
        //    //        graph.Add(currentSegment.Value);
        //    //        currentSegment = currentSegment.NextSegment ?? (stack.Count > 0 ? stack.Pop() : null);
        //    //    }

        //    //    done = stack.Count == 0 && currentSegment == null;// && currentSegment?.NextSegment == null;
        //    //}

        //    return graph;
        //}

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return this.Id == (obj as Segment)?.Id;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }

        public void OnDeserialization(object sender)
        {
            throw new NotImplementedException();
        }
    }
}
