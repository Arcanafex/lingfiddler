﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpigrapherWorkshop
{
    /// <summary>
    /// A collection of SegmentValues.
    /// </summary>
    public class Lexicon : ICollection<SegmentValue>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public int Count => Collection.Count();
        public bool IsReadOnly => ReadOnly;
        public bool ReadOnly { get; set; }

        private HashSet<SegmentValue> Collection { get; set; }

        private Lexicon()
        {
            Collection = new HashSet<SegmentValue>();
        }

        public static Lexicon Create(string name)
        {
            return new Lexicon() { Name = name };
        }

        public void Add(SegmentValue item)
        {
            Collection.Add(item);
        }

        public void Clear()
        {
            Collection.Clear();
        }

        public bool Contains(SegmentValue item)
        {
            return Collection.Contains(item);
        }

        public void CopyTo(SegmentValue[] array, int arrayIndex)
        {
            var codex = Collection.ToArray();

            for (int i = 0; i < codex.Length && (i + arrayIndex) < array.Length; i++)
            {
                array[arrayIndex + i] = codex[i];
            }
        }

        public bool Remove(SegmentValue item)
        {
            return Collection.Remove(item);
        }

        public IEnumerator<SegmentValue> GetEnumerator()
        {
            return Collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Collection.GetEnumerator();
        }
    }
}
