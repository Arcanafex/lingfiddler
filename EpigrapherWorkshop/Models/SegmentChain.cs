﻿using System;
using System.Collections.Generic;

namespace EpigrapherWorkshop
{
    public class SegmentChain : LinkedList<Segment>
    {
        public Guid Id { get; set; }

        private SegmentChain()
        {
            Id = Guid.NewGuid();
        }

        public static SegmentChain Create(Segment segment)
        {
            var chain = new SegmentChain();
            chain.AddLast(segment);

            return chain;
        }

        public static SegmentChain Create(Segment[] segments)
        {
            var chain = new SegmentChain();

            foreach(var segment in segments)
            {
                chain.AddLast(segment);
            }

            return chain;
        }
        public static SegmentChain Create(string value, Layer layer = null)
        {
            var chain = new SegmentChain();
            chain.AddLast(Segment.Create(value, layer));

            return chain;
        }

        public static SegmentChain Create(string[] values, Layer layer = null)
        {
            var chain = new SegmentChain();

            foreach(var value in values)
            {
                chain.AddLast(Segment.Create(value, layer));
            }

            return chain;
        }

        public SegmentChain Split(int index)
        {
            var segments = new Queue<Segment>(this);
            Clear();

            for(int i = 0; i <= index; i++)
            {
                AddLast(segments.Dequeue());
            }

            var splitChain = Create(segments.ToArray());

            return splitChain;
        }

        public void Append(SegmentChain chain)
        {
            if (chain == null)
                return;

            foreach(var segment in chain)
            {
                AddLast(segment);
            }
        }
    }
}
