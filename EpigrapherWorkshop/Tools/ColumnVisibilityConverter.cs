﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Width = System.Double;

namespace EpigrapherWorkshop
{
    public class ColumnVisibilityManager
    {
        private static readonly Dictionary<GridViewColumn, Width> m_originalColumnWidths = new Dictionary<GridViewColumn, Width>();
        private static readonly DependencyProperty m_isVisibleProperty = DependencyProperty.RegisterAttached("IsVisible", typeof(bool), typeof(ColumnVisibilityManager), new UIPropertyMetadata(true, OnIsVisibleChanged));

        public static DependencyProperty IsVisibleProperty { get => m_isVisibleProperty; }

        public static bool GetIsVisible(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsVisibleProperty);
        }

        public static void SetIsVisible(DependencyObject obj, bool value)
        {
            obj.SetValue(IsVisibleProperty, value);
        }

        private static void OnIsVisibleChanged(DependencyObject dependency, DependencyPropertyChangedEventArgs e)
        {
            GridViewColumn gridColumnView = dependency as GridViewColumn;

            if (gridColumnView == null)
                return;

            if (GetIsVisible(gridColumnView) == false)
            {
                m_originalColumnWidths[gridColumnView] = gridColumnView.Width;
                gridColumnView.Width = 0;
            }
            else
            {
                if (gridColumnView.Width == 0)
                    gridColumnView.Width = m_originalColumnWidths[gridColumnView];
            }
        }
    }
}
