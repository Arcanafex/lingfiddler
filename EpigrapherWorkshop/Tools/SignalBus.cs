﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpigrapherWorkshop
{
    public class SignalBus
    {
        private static SignalBus instance;
        private static SignalBus s_instance
        {
            get
            {
                if (instance == null)
                    instance = new SignalBus();

                return instance;
            }

            set => instance = value;
        }

        private SignalBus()
        {
            m_RegisteredListeners = new List<Listener>();
        }

        private List<Listener> m_RegisteredListeners { get; set; }

        public static void Fire(object signal)
        {
            //if (s_instance.m_RegisteredListeners.TryGetValue(signal.GetType(), out ISignalHandler handler))
            foreach(var handler in s_instance.m_RegisteredListeners.Where(listener => listener.Type == signal.GetType()).Select(listener => listener.Handler))
            {
                handler.Invoke(signal);
            }
        }

        public static void RegisterListener<T>(Action<T> action)
        {
            s_instance.m_RegisteredListeners.Add(new Listener(typeof(T), SignalHandler<T>.Create(action)));
        }

        private class Listener
        {
            public Type Type { get; set; }
            public ISignalHandler Handler { get; set; }

            public Listener(Type type, ISignalHandler handler)
            {
                Type = type;
                Handler = handler;
            }
        }
    }

    public interface ISignalHandler
    {
        void Invoke(object signal);
    }

    public class SignalHandler<T> : ISignalHandler
    {
        public static SignalHandler<T> Create(Action<T> action)
        {
            var signal = new SignalHandler<T>();
            signal.m_action = action;
            return signal;
        }

        private Action<T> m_action { get; set; }

        public void Invoke(object signal)
        {
            m_action.Invoke((T)signal);
        }
    }
}
