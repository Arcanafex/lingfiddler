﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpigrapherWorkshop
{
    public class AddSegmentValueToLexiconSignal
    {
        public SegmentValue SegmentValue { get; set; }
        public Lexicon Lexicon { get; set; }

        public AddSegmentValueToLexiconSignal(SegmentValue segmentValue, Lexicon lexicon = null)
        {
            SegmentValue = segmentValue;
            Lexicon = lexicon;
        }
    }
}
