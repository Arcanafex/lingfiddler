﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpigrapherWorkshop
{
    public static class Tools
    {
        public static string ExampleExpression = "The quick brown fox jumped over the lazy dog.";
        public static string[] ExampleWords = { "The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog" };
        public static string ExampleText = "Cette histoire n'est pas fantastique, elle n'est que romanesque. Faut-il en conclure qu'elle ne soit pas vraie, étant donné son invraisemblance ? Ce serait une erreur. Nous sommes d'un temps où tout arrive, -- on a presque le droit de dire où tout est arrivé. Si notre récit n'est point vraisemblable aujourd'hui, il peut l'être demain, grâce aux ressources scientifiques qui sont le lot de l'avenir, et personne ne s'aviserait de le mettre au rang des légendes. D'ailleurs, il ne se crée plus de légendes au déclin de ce pratique et positif XIXe siècle, ni en Bretagne, la contrée des farouches korrigans, ni en Ecosse, la terre des brownies et des gnomes, ni en Norvège, la patrie des ases, des elfes, des sylphes et des valkyries, ni même en Transylvanie, où le cadre des Carpathes se prête si naturellement à toutes les évocations psychagogiques. Cependant il convient de noter que le pays transylvain est encore très attaché aux superstitions des premiers âges.";

        public static Segment GenerateTestExpressionChain()
        {
            string[] words = { "The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog" };
            var root = Segment.Create(words);
            return root;
        }

        public static Segment GenerateFacadeSegment(int chainLength = 0)
        {
            var segment = Segment.Create("TOKEN TEXT");

            if (chainLength > 0)
            {
                var value = segment.Analyze();

                for (int i = 0; i < chainLength; i++)
                {
                    var child = Segment.Create($"S{i + 1}");
                    value.Composition.AddLast(child);

                    if (i % 3 == 0)
                    {
                        var childValue = child.Analyze();

                        for (int j = 0; j < chainLength - 1; j++)
                        {
                            childValue.Composition.AddLast(Segment.Create($"S{j + 1}"));
                        }
                    }
                }
            }
            return segment;
        }
    }
}
