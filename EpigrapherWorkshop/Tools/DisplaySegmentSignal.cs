﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpigrapherWorkshop
{
    public class DisplaySegmentSignal
    {
        public Segment Segment { get; set; }

        public DisplaySegmentSignal(Segment segment)
        {
            Segment = segment;
        }
    }
}
