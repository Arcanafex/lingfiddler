﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace EpigrapherWorkshop
{
    public class ColumnStarWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var listView = value as ListView;
            var width = listView.Width;
            var gridView = listView.View as GridView;

            for (int i = 0; i < gridView.Columns.Count; i++)
            {
                if (!double.IsNaN(gridView.Columns[i].Width))
                    width -= gridView.Columns[i].Width;
            }
            return width - 5;// this is to take care of margin/padding
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
