﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpigrapherWorkshop
{
    public class AddSegmentToCodexSignal
    {
        public Segment Segment { get; set; }
        public Codex Codex { get; set; }

        public AddSegmentToCodexSignal(Segment segment, Codex codex = null)
        {
            Segment = segment;
            Codex = codex;
        }

    }
}
