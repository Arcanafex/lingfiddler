﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EpigrapherWorkshop
{
    /// <summary>
    /// Interaction logic for PopupDialog.xaml
    /// </summary>
    public partial class PopupMessage : Window
    {
        public string PopupTitle { get; set; }
        public string TextContent { get; set; }
        public ICommand CommandAccept { get; set; }

        public PopupMessage(string title, string content)
        {
            InitializeComponent();

            PopupTitle = title;
            TextContent = content;

            CommandAccept = new CommandDelegate((param) => { Close(); });
            DataContext = this;
        }
    }
}
