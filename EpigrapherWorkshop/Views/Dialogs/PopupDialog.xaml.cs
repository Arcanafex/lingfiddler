﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EpigrapherWorkshop
{
    /// <summary>
    /// Interaction logic for PopupDialog.xaml
    /// </summary>
    public partial class PopupDialog : Window
    {
        public string PopupTitle { get; set; }
        public string TextContent { get; set; }
        public ICommand CommandAccept { get; set; }
        public ICommand CommandCancel { get; set; }

        public PopupDialog(string title, string initialContent)
        {
            InitializeComponent();

            PopupTitle = title;
            TextContent = initialContent;

            CommandCancel = new CommandDelegate((param) => { Close(); });
            CommandAccept = new CommandDelegate(Accept);
            DataContext = this;
        }

        public void Accept(object obj)
        {
            DialogResult = true;
            Close();
        }
    }
}
