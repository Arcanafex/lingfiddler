﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ookii.Dialogs.Wpf;

namespace Lexicon_Scraper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainViewModel m_view;

        public MainWindow()
        {
            InitializeComponent();
            m_view = new MainViewModel();
            DataContext = m_view;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            VistaFolderBrowserDialog openFolderDialog = new VistaFolderBrowserDialog();

            if (openFolderDialog.ShowDialog() == true)
            {
                m_view.Destination = openFolderDialog.SelectedPath;
            }
        }
    }

    /// <summary>
    /// https://stackoverflow.com/questions/1043918/open-file-dialog-mvvm
    /// </summary>
    public class DialogHelper : DependencyObject
    {
        public MainViewModel ViewModel
        {
            get { return (MainViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainViewModel), typeof(DialogHelper),
            new UIPropertyMetadata(new PropertyChangedCallback(ViewModelProperty_Changed)));

        private static void ViewModelProperty_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (ViewModelProperty != null)
            {
                Binding myBinding = new Binding("FileName");
                myBinding.Source = e.NewValue;
                myBinding.Mode = BindingMode.OneWayToSource;
                BindingOperations.SetBinding(d, FileNameProperty, myBinding);
            }
        }

        private string FileName
        {
            get { return (string)GetValue(FileNameProperty); }
            set { SetValue(FileNameProperty, value); }
        }

        private static readonly DependencyProperty FileNameProperty =
            DependencyProperty.Register("FileName", typeof(string), typeof(DialogHelper),
            new UIPropertyMetadata(new PropertyChangedCallback(FileNameProperty_Changed)));

        private static void FileNameProperty_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //Debug.WriteLine("DialogHelper.FileName = {0}", e.NewValue);
        }

        public ICommand OpenFile { get; private set; }

        public DialogHelper()
        {
            OpenFile = new CommandDelegate(OpenFileAction);
        }

        private void OpenFileAction(object obj)
        {
            VistaFolderBrowserDialog dlg = new VistaFolderBrowserDialog();

            if (dlg.ShowDialog() == true)
            {
                FileName = dlg.SelectedPath;
            }
        }
    }
}
