﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lexicon_Scraper
{
    public class MainViewModel : NotifyPropertyChangedBase
    {
        public static HttpClient Client = new HttpClient();
        public static List<string> ConsoleLog { get; set; } = new List<string>();

        public string Address
        {
            get
            {
                if (string.IsNullOrEmpty(m_address))
                    m_address = Properties.Settings.Default.Address;

                return m_address;
            }
            set
            {
                m_address = value;
                OnPropertyChanged(nameof(Address));
            }
        }
        public string Destination
        {
            get
            {
                if (string.IsNullOrEmpty(m_destination))
                {
                    m_destination = Properties.Settings.Default.Destination;
                    System.IO.Directory.SetCurrentDirectory(m_destination);
                }

                return m_destination;
            }
            set
            {
                m_destination = value;
                OnPropertyChanged(nameof(Destination));
            }
        }

        private string m_address;
        private string m_destination;

        public bool SelectingAll { get; set; }
        public string Pattern { get; set; }
        public string MessageDisplay
        {
            get => string.Join("\n", ConsoleLog.ToArray());
            //set
            //{
            //    m_addresses.Clear();

            //    foreach (string line in value.Split(new string[]{ "\n" }, StringSplitOptions.RemoveEmptyEntries))
            //    {
            //        m_addresses.Enqueue(line);
            //    }

            //    OnPropertyChanged(nameof(MessageDisplay));
            //}
        }

        public ObservableCollection<AssetModel> Findings { get; set; }

        public ICommand Search { get; set; }
        public ICommand Export { get; set; }
        public ICommand BrowsePath { get; set; }
        public ICommand SelectAll { get; set; }

        private Regex AddressPattern;
        private Regex PathPattern;
        private Regex DataPattern;

        private Queue<string> m_addresses;

        public MainViewModel()
        {
            Findings = new ObservableCollection<AssetModel>();
            m_addresses = new Queue<string>();

            Search = new CommandDelegate(SearchCommand);
            SelectAll = new CommandDelegate(SelectAllCommand);
            Export = new CommandDelegate(ExportCommand);


            AddressPattern = new Regex("src=\"(.*?\\.svg)", RegexOptions.IgnoreCase);
            PathPattern = new Regex("<path.*?>", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            DataPattern = new Regex("\\sd=\"(.*?)\"", RegexOptions.IgnoreCase | RegexOptions.Singleline);
        }

        private void SearchCommand(object param)
        {
            m_addresses.Clear();
            ConsoleLog.Clear();
            Findings.Clear();

            OnPropertyChanged(nameof(MessageDisplay));
            OnPropertyChanged(nameof(Findings));

            var searchTask = ExecuteSearch();
        }

        private void SelectAllCommand(object param)
        {
            SelectingAll = !SelectingAll;

            foreach(var item in Findings)
            {
                item.Select(SelectingAll);
            }
        }

        private void ExportCommand(object param)
        {
            if (!System.IO.Directory.Exists((string)param))
            {
                try
                {
                    System.IO.Directory.CreateDirectory((string)param);
                }
                catch (ArgumentException e)
                {
                    return;
                }
            }

            try
            {
                System.IO.Directory.SetCurrentDirectory((string)param);
            }
            catch (ArgumentException e)
            {
                return;
            }


            Properties.Settings.Default.Destination = System.IO.Directory.GetCurrentDirectory();
            Properties.Settings.Default.Save();

            foreach(var item in Findings)
            {
                if (item.Selected)
                    item.Fetch.Execute(null);
            }
        }

        public void SetDestinationPath(string path)
        {
            Destination = path;
            OnPropertyChanged(nameof(Destination));
        }

        private async Task ExecuteSearch()
        {
            try
            {
                HttpResponseMessage response = await Client.GetAsync(Address);
                response.EnsureSuccessStatusCode();
                Properties.Settings.Default.Address = Address;
                Properties.Settings.Default.Save();

                string responseBody = await response.Content.ReadAsStringAsync();
                await CrawlAddress(responseBody);
            }
            catch (HttpRequestException e)
            {
                //MessageDisplay = "\nException Caught!";
                //MessageDisplay = $"Message :{e.Message}";
            }
        }

        private void WriteToDisplay(string line)
        {
            ConsoleLog.Add(line);
            OnPropertyChanged(nameof(MessageDisplay));
        }

        private async Task CrawlAddress(string content)
        {
            foreach(Match match in AddressPattern.Matches(content))
            {
                string address = match.Groups[1].Value.Trim('"');
                address = address.Replace(@"/thumb", "");

                if (!address.StartsWith("http"))
                {
                    address = $"https://{address.Trim('/')}";
                }

                m_addresses.Enqueue(address);
            }


            while (m_addresses.Count > 0)
            {
                string nextAddress = m_addresses.Dequeue();

                try
                {
                    HttpResponseMessage response = await Client.GetAsync(nextAddress);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    WriteToDisplay(nextAddress);

                    foreach(Match path in PathPattern.Matches(responseBody))
                    {
                        foreach(Match data in DataPattern.Matches(path.Value))
                        {
                            var assetModel = new AssetModel(nextAddress)
                            {
                                Data = data.Groups[1].Value,
                                BinaryData = await response.Content.ReadAsByteArrayAsync()
                            };

                            Findings.Add(assetModel);
                        }
                    }

                    OnPropertyChanged(nameof(Findings));

                    await CrawlAddress(responseBody);
                }
                catch (HttpRequestException e)
                {
                    WriteToDisplay($"*NOPE* {nextAddress}");
                }
            }
        }
    }

    public class AssetModel : NotifyPropertyChangedBase
    {
        public string Address { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public bool Selected { get; set; }
        public byte[] BinaryData { get; set; }
        public ICommand Fetch { get; set; }
        public bool Fetched => System.IO.File.Exists(ExportPath);

        private Task RequestTask { get; set; }
        private string ExportPath => System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), Name);

        public AssetModel(string address)
        {
            Address = address;
            Name = address.Substring(address.LastIndexOf('/') + 1);
            Fetch = new CommandDelegate(FetchCommand);
        }

        public void Select(bool selected)
        {
            Selected = selected;
            OnPropertyChanged(nameof(Selected));
        }

        public void FetchCommand(object param)
        {
            RequestTask = FetchAsset();
        }

        public async Task FetchAsset()
        {
            if (BinaryData == null)
            {
                try
                {
                    HttpResponseMessage response = await MainViewModel.Client.GetAsync($"{Address}/{Name}");
                    response.EnsureSuccessStatusCode();

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        BinaryData = await response.Content.ReadAsByteArrayAsync();

                        //// Start fetch
                        //await response.Content.CopyToAsync(new System.IO.FileStream($"{ExportPath}", System.IO.FileMode.Create));
                    }
                }
                catch (HttpRequestException e)
                {
                }
            }

            using (var writer = new System.IO.BinaryWriter(new System.IO.FileStream($"{ExportPath}", System.IO.FileMode.Create)))
            {
                try
                {
                    writer.Write(BinaryData);
                    OnPropertyChanged(nameof(Fetched));
                }
                catch (Exception e)
                { }
            }
        }
    }

    /// <summary>
    /// Base class implementing INotifyPropertyChanged
    /// </summary>
    public class NotifyPropertyChangedBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

    /// <summary>
    /// Command delegate implementation for handling UI Commands.
    /// </summary>
    public class CommandDelegate : ICommand
    {
        public CommandDelegate(Action<object> execute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }

            m_execute = execute;
        }

        public CommandDelegate(Func<object, bool> canExecute, Action<object> execute)
        {
            if (canExecute == null)
            {
                throw new ArgumentNullException(nameof(canExecute));
            }
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }

            m_canExecute = canExecute;
            m_execute = execute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return m_canExecute == null || m_canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            m_execute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, System.EventArgs.Empty);
            }
        }

        #region Private members
        private Func<object, bool> m_canExecute;
        private Action<object> m_execute;
        #endregion
    }
}
