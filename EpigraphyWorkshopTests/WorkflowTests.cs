using NUnit.Framework;
using EpigrapherWorkshop;

namespace EpigraphyWorkshopTests
{
    public class WorkflowTests
    {
        [SetUp]
        public void Setup()
        {
            var corpus = Corpus.Create("Test Corpus");
            Corpus.SetCurrentCorpus(corpus);
        }

        string TestText = "The quick brown fox jumped over the lazy dog. Then it kicked it.";

        string[] ExpressionText =
        {
            "The quick brown fox jumped over the lazy dog.",
            "Then it kicked it."
        };

        string[][] WordText =
        {
            new string[] { "The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog." },
            new string[] { "Then", "it", "kicked", "it." }
        };

        [Test]
        public void Corpus_InitializationCheck()
        {
            Assert.NotNull(Corpus.Current);
            Assert.Greater(Corpus.Current.CodexCount, 0);
            Assert.Greater(Corpus.Current.LayerCount, 0);
        }

        [Test]
        public void Corpus_NewText()
        {
            var text = Segment.Create(TestText);
            var texts = Corpus.Current.GetCodex("Texts");

            // Adding the text segment to the Texts codex
            texts.Add(text);

            Assert.AreEqual(texts.Count, 1);
            Assert.AreEqual(TestText, text.Value.ToString());
        }

        [Test]
        public void Corpus_NewText_AddLayer()
        {
            var text = Segment.Create(TestText);

            Corpus.Current.DefineLayer("Test");
            var testLayer = Corpus.Current.GetLayer("Test");
            
            Assert.NotNull(testLayer);

            string testValue = "This is a test layer";
            text[testLayer] = testValue;

            Assert.AreEqual(testValue, text[testLayer].ToString());
            Assert.AreEqual(TestText, text.Value.ToString());
            Assert.AreEqual(TestText, text[Corpus.DefaultLayer].ToString());
        }

        [Test]
        public void Corpus_NewText_SetComposition()
        {
            var text = Segment.Create(TestText);

            Assert.AreEqual(TestText, text.Value.ToString());
            Assert.AreEqual(TestText, text[Corpus.DefaultLayer].ToString());

            text[Corpus.DefaultLayer].Composition = SegmentChain.Create(ExpressionText);

            Assert.AreEqual(ExpressionText.Length, text[Corpus.DefaultLayer].Composition.Count);
            Assert.AreEqual(ExpressionText.Length, text.Value.Composition.Count);

            int i = 0;
            foreach (var expression in text[Corpus.DefaultLayer].Composition)
            {
                Assert.AreEqual(ExpressionText[i], expression[Corpus.DefaultLayer].ToString());
                Assert.AreEqual(ExpressionText[i], expression.Value.ToString());

                expression.Value = WordText[i];

                int j = 0;
                foreach (var word in expression.Value.Composition)
                {
                    Assert.AreEqual(WordText[i][j], word[Corpus.DefaultLayer].ToString());
                    Assert.AreEqual(WordText[i][j], word.Value.ToString());

                    j++;
                }

                i++;
            }
        }

        [Test]
        public void Corpus_NewText_PopulateCodex()
        {
            string codex1 = "Expressions";
            string codex2 = "Words";

            var text = Segment.Create(TestText);
            text.Value.Composition = SegmentChain.Create(ExpressionText);

            Corpus.Current.DefineCodex(codex1);
            Corpus.Current.DefineCodex(codex2);

            var expressions = Corpus.Current.GetCodex(codex1);
            var words = Corpus.Current.GetCodex(codex2);

            int i = 0;
            foreach (var expression in text.Value.Composition)
            {
                expressions.Add(expression);
                expression.Value = WordText[i++];

                foreach (var word in expression.Value.Composition)
                {
                    words.Add(word);
                }
            }

            foreach (var expression in text.Value.Composition)
            {
                Assert.IsTrue(expressions.Contains(expression));

                foreach (var word in expression.Value.Composition)
                {
                    Assert.IsTrue(words.Contains(word));
                }
            }
        }

        [Test]
        public void Corpus_Segments_RegexAnalyze()
        {
            var text = Segment.Create();
            text.Value = TestText;

            var texts = Corpus.Current.GetCodex("Texts");
            var expressions = Codex.Create("Expressions");
            var words = Codex.Create("Words");

            texts.Add(text);

            var expressionRegex = new System.Text.RegularExpressions.Regex(@"\w+.*?\.");
            var wordRegex = new System.Text.RegularExpressions.Regex(@"\w+");

            var textValue = text.Analyze(Corpus.DefaultLayer, expressionRegex);
            
            Assert.AreEqual(2, textValue.Composition.Count);
            Assert.AreEqual(2, text[Corpus.DefaultLayer].Composition.Count);

            int i = 0;
            foreach(var expression in text.Value.Composition)
            {
                expressions.Add(expression);

                var wordText = WordText[i++];
                var value = expression.Analyze(Corpus.DefaultLayer, wordRegex);
                Assert.AreEqual(wordText.Length, value.Composition.Count);

                foreach(var word in value.Composition)
                {
                    words.Add(word);
                }
            }
        }

        [Test]
        public void Corpus_Segments_Analyze()
        {
            var text = Segment.Create();
            text.Value = TestText;

            var texts = Corpus.Current.GetCodex("Texts");
            var expressions = Codex.Create("Expressions");
            var words = Codex.Create("Words");

            texts.Add(text);

            var textValue = text.Analyze();

            Assert.AreEqual(1, textValue.Composition.Count);

            textValue.Composition.Clear();

            foreach (var exp in ExpressionText)
            {
                var expression = Segment.Create(exp);
                textValue.Composition.AddLast(expression);
                expressions.Add(expression);
            }

            Assert.AreEqual(2, textValue.Composition.Count);
            Assert.AreEqual(2, text.Value.Composition.Count);
            Assert.AreEqual(2, text[Corpus.DefaultLayer].Composition.Count);

            int i = 0;
            foreach (var expression in text.Value.Composition)
            {
                var wordText = WordText[i++];
                var value = expression.Analyze(null, " ");
                Assert.AreEqual(wordText.Length, value.Composition.Count);

                foreach (var word in value.Composition)
                {
                    words.Add(word);
                }
            }

            foreach (var word in words)
            {
                var charVals = word.Analyze(null, "".ToCharArray());
                Assert.AreEqual(charVals.Graph.Length, charVals.Composition.Count);
            }
        }

        //[Test]
        public void Segments_Nesting()
        {
            var layer = Layer.Create("Surface");

            // X -> O -> F
            //      O -> D -> E
            //      O -> C
            //      O
            //      O -> A -> B
            //
            // Root             = ABCDE
            // Root + Chain     = ABCDEF
            // PreRoot + Chain  = XABCDEF
            // PreRoot          = X

            //var root = Segment.Create();
            ////root.Layer = layer;

            //var preRoot = Segment.Create();
            //preRoot.Value = "X";
            ////root.InsertBefore(preRoot);

            //var postRoot = Segment.Create();
            //postRoot.Value = "F";

            ////root.InsertAfter(postRoot);

            ////var level1 = root.Analyze();
            //var segmentD = Segment.Create();
            ////segmentD.Layer = level1.Layer;
            //segmentD.Value = "D";
            ////level1.InsertAfter(segmentD);
            //var segmentE = Segment.Create();
            ////segmentE.Layer = level1.Layer;
            //segmentE.Value = "E";
            ////segmentD.InsertAfter(segmentE);

            ////var level2 = level1.Analyze();
            //var segmentC = Segment.Create();
            ////segmentC.Layer = level1.Layer;
            //segmentC.Value = "C";
            ////level2.InsertAfter(segmentC);

            ////var level3 = level2.Analyze();

            //var segmentA = level3.Analyze();
            //segmentA.Value = "A";
            //var segmentB = Segment.Create();
            ////segmentB.Layer = level1.Layer;
            //segmentB.Value = "B";
            ////segmentA.InsertAfter(segmentB);

            ////Assert.AreEqual("XABCDEF", preRoot.ToString(true));
            ////Assert.AreEqual("X", preRoot.ToString());

            ////Assert.AreEqual("ABCDEF", root.ToString(true));
            ////Assert.AreEqual("ABCDE", root.ToString());

            ////Assert.AreEqual("F", postRoot.ToString(true));
            ////Assert.AreEqual("F", postRoot.ToString());

        }
    }
}