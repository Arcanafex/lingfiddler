using NUnit.Framework;
using EpigrapherWorkshop;

namespace EpigraphyWorkshopTests
{
    public class SegmentTests
    {
        [SetUp]
        public void Setup()
        {
            var corpus = Corpus.Create("Test Corpus");
            Corpus.SetCurrentCorpus(corpus);
        }

        string[] SegmentValues =
        {
            "A", "B", "C", "D"
        };

        [Test]
        public void Segments_Analyze()
        {
            //var layer = Layer.Create("Surface");
            //layer.SegmentSeparator = " ";

            //var text = Segment.Create();
            //text.Value = TestText;
            //text.Layer = layer;

            //var texts = Corpus.Current.GetCodex("Texts");
            //var expressions = Codex.Create("Expressions");
            //var words = Codex.Create("Words");

            //var expression = text.Analyze();
            //var nextExpression = expression.Split(expression.Value.IndexOf(".") + 1);
            //nextExpression.Value = nextExpression.Value.Trim();

            //texts.Add(text);
            //expressions.Add(expression);
            //expressions.Add(nextExpression);

            //var word = expression.Analyze();

            //while (word.Value.IndexOf(" ") > -1)
            //{
            //    words.Add(word);

            //    word = word.Split(word.Value.IndexOf(" "));
            //    word.Value = word.Value.Trim();
            //}

            //words.Add(word);

            //Assert.AreEqual(9, words.Count);

            //var output = text.ToString();

            //Assert.AreEqual(TestText, output);
        }

        [Test]
        public void Segments_Nesting()
        {
            var layer = Layer.Create("Surface");

            // X -> O -> F
            //      O -> D -> E
            //      O -> C
            //      O
            //      O -> A -> B
            //
            // Root             = ABCDE
            // Root + Chain     = ABCDEF
            // PreRoot + Chain  = XABCDEF
            // PreRoot          = X

            //var root = Segment.Create();
            //root.Layer = layer;

            //var preRoot = Segment.Create();
            //preRoot.Value = "X";
            //root.InsertBefore(preRoot);

            //var postRoot = Segment.Create();
            //postRoot.Value = "F";

            //root.InsertAfter(postRoot);

            //var level1 = root.Analyze();
            //var segmentD = Segment.Create();
            //segmentD.Layer = level1.Layer;
            //segmentD.Value = "D";
            //level1.InsertAfter(segmentD);
            //var segmentE = Segment.Create();
            //segmentE.Layer = level1.Layer;
            //segmentE.Value = "E";
            //segmentD.InsertAfter(segmentE);

            //var level2 = level1.Analyze();
            //var segmentC = Segment.Create();
            //segmentC.Layer = level1.Layer;
            //segmentC.Value = "C";
            //level2.InsertAfter(segmentC);

            //var level3 = level2.Analyze();

            //var segmentA = level3.Analyze();
            //segmentA.Value = "A";
            //var segmentB = Segment.Create();
            //segmentB.Layer = level1.Layer;
            //segmentB.Value = "B";
            //segmentA.InsertAfter(segmentB);

            //Assert.AreEqual("XABCDEF", preRoot.ToString(true));
            //Assert.AreEqual("X", preRoot.ToString());

            //Assert.AreEqual("ABCDEF", root.ToString(true));
            //Assert.AreEqual("ABCDE", root.ToString());

            //Assert.AreEqual("F", postRoot.ToString(true));
            //Assert.AreEqual("F", postRoot.ToString());

        }
    }
}